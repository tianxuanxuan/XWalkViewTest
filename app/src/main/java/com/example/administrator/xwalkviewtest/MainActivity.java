package com.example.administrator.xwalkviewtest;

import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.administrator.xwalkviewtest.JS_Bind;
import com.example.administrator.xwalkviewtest.R;

import org.xwalk.core.JavascriptInterface;
import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkView;

/**
 * Created by Administrator on 2017/9/21.
 */

public class MainActivity extends AppCompatActivity implements CallBack{
    public static String Aid;
    private XWalkView xWalkView;
    private JS_Bind jsBind;
    JS_Bind js_bind;


    @Override
    protected void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.layout_videoactivity);


        //js_bind1.showInfoFromJs("123",Main);


        Aid = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

        XWalkPreferences.setValue("enable-javascript",true);
        XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING,true);
        // XWalXWalkSettings mWebSettings = mWebView.getSettings();
        //xWalkView.getSettings().setSupportZoom(false);

        //XWalkPreferences.setValue(XWalkPreferences);

        xWalkView = (XWalkView)findViewById(R.id.xw);



        js_bind = new JS_Bind(this,xWalkView);
        js_bind.setName("tainxuan");
        js_bind.setAge("24");
        js_bind.setSex("男");
        xWalkView.addJavascriptInterface(js_bind,"person");
        xWalkView.getSettings().setSupportZoom(true);

        //xWalkView.addJavascriptInterface(new JsInterface(), "NativeInterface");
       // xWalkView.loadUrl("javascript:callJS()");
        //xWalkView.loadUrl("file:///android_asset/index.html");
        //xWalkView.load("file:///android_asset/index.html",null);
      //https://192.168.1.116:8443
        xWalkView.load("file:///android_asset/index.html",null);

        js_bind.setInterface(new CallBack() {
            @Override
            public void solve(String string) {
                System.out.println("田宣哈哈"+string);
            }
        });



    }
    @Override
    protected void onStart(){
        super.onStart();


    }

    @Override
    protected void onPause(){
        super.onPause();
        if (xWalkView!=null){
            xWalkView.pauseTimers();
            xWalkView.onHide();
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        if (xWalkView!=null){
            xWalkView.resumeTimers();
            xWalkView.onShow();
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (xWalkView!=null){
            xWalkView.onDestroy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        if (xWalkView!=null){
            xWalkView.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    protected void onNewIntent(Intent intent){
        if (xWalkView!=null){
            xWalkView.onNewIntent(intent);
        }
    }

    @Override
    public void solve(String string) {
        Toast.makeText(this,"接收到了回调"+ string,Toast.LENGTH_LONG).show();
    }

    public class JsInterface {
        public JsInterface() {
        }

        @JavascriptInterface
        public String sayHello() {
            // TODO do more thing
            return "Hello World!";
        }
    }


}


