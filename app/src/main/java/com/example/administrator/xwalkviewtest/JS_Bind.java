package com.example.administrator.xwalkviewtest;

import android.content.Context;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.xwalk.core.JavascriptInterface;
import org.xwalk.core.XWalkView;

import java.util.ArrayList;
import java.util.List;

import static org.chromium.base.ContextUtils.getApplicationContext;
import static org.chromium.base.ThreadUtils.runOnUiThread;



/**
 * Created by Administrator on 2017/9/21.
 */

public class JS_Bind{
    private static final String TAG = "JS_Bind";
    private Context context;
    private XWalkView xWalkView;
    public CallBack callBack;

    private Toast mToast;


    public JS_Bind(Context c, XWalkView xWalkView){
        context = c;
        this.xWalkView =xWalkView;
    }

    public void setInterface(CallBack callBack){
        this.callBack = callBack;
    }

    void setCallFun(CallBack callFun){
        this.callBack = callFun;
    }

    private String name;
    private String age;
    private String sex;

    @JavascriptInterface
    public String getAge() {
        return age;
    }
    @JavascriptInterface
    public void setAge(String age) {
        this.age = age;
    }
    @JavascriptInterface
    public void setSex(String sex) {
        this.sex = sex;
    }

    @JavascriptInterface
    public String getSex() {
        String sex = "男";
        return sex;
    }

    @JavascriptInterface
    public String getName() {
        return name;
    }
    @JavascriptInterface
    public void setName(String name) {
        this.name = name;
    }

    @JavascriptInterface
    public void showInfoFromJs(String name) {
        callBack.solve(name);
        showTip(name);
    }


    private void showTip(final String str) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mToast == null) {
                    mToast = Toast.makeText(getApplicationContext(), "",
                            Toast.LENGTH_LONG);
                    LinearLayout layout = (LinearLayout) mToast.getView();
                    TextView tv = (TextView) layout.getChildAt(0);
                    tv.setTextSize(25);
                }
                //mToast.cancel();
                mToast.setGravity(Gravity.CENTER, 0, 0);
                mToast.setText(str);
                mToast.show();
            }
        });
    }

}
